﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace casting3
{
    class Program
    {
        static void DrawShapeMaybeIn3DAndMaybeNot(Shape shape)
        {
            if (shape is ShapeUnderConstructionDontDraw)
            {
                Console.WriteLine("under construction");
                return;
            }
            shape.Draw();
            IDraw3D shape_support_draw3d = shape as IDraw3D;
            if (shape_support_draw3d != null)
            {
                shape_support_draw3d.DrawIn3D();
            }
        }
        static void Main(string[] args)
        {
        }
    }
}
