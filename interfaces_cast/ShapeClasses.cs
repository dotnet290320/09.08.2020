﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace casting3
{
    public interface ShapeUnderConstructionDontDraw
    {

    }
    public interface IDraw
    {
        void Draw();
    }
    public interface IDraw3D
    {
        void DrawIn3D();
    }
    public abstract class Shape : IDraw
    {
        public string Name { get; protected set; }
        public Shape(string name)
        {
            Name = name;
        }

        public abstract void Draw();
    }
    public class Circle : Shape, ShapeUnderConstructionDontDraw
    {
        public Circle(string name) : base(name)
        {
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing circle");
        }
    }
    public class Rectangle : Shape, IDraw3D
    {
        public Rectangle(string name) : base(name)
        {
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing rectangle");
        }

        public void DrawIn3D()
        {
            Console.WriteLine("Drawing rectangle in 3D! wow!!");
        }
    }
}
