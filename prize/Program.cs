﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace casting2
{
    class Program
    {
        static void GuessPrize(Prize p)
        {
            Console.WriteLine("Choose which prize. if you choose correctly you will win [1-3]");
            Console.WriteLine("1 - money voucher");
            Console.WriteLine("2 - cruize");
            Console.WriteLine("3 - horse riding");

            Console.WriteLine($"hint: {p.Hint}");

            int c = Convert.ToInt32(Console.ReadLine());
            // ... complete
            // if the user clicked '1' check for exmaple if p is money voucher
            // if so, print "winner!" and print the amount of money he won!
            // do the same for '2', '3'
        }
        static void Main(string[] args)
        {
            Random r = new Random();
            int choise = r.Next(1, 3);
            Prize p = null;
            switch (choise)
            {
                case 1:
                    p = new MoneyVoucher(1_000_000, "expensive");
                    break;
                case 2:
                    p = new CruizeCaribbean(r.Next(1,100), "expensive");
                    break;
                case 3:
                    p = new HorseRiding(r.Next(30, 60), "expensive");
                    break;
            }
            GuessPrize(p);
        }
    }
}
