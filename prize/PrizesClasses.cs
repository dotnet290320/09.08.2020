﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace casting2
{
   public abstract class Prize
    {
        public string Hint { get; protected set; }
        public Prize(string hint)
        {
            Hint = hint;
        }
    }
    public class MoneyVoucher : Prize
    {
        public int Amount { get; private set; }
        public MoneyVoucher(int amount, string hint) : base(hint)
        {
            Amount = amount;
        }
    }
    public class CruizeCaribbean : Prize
    {
        public int CabinNumber { get; private set; }
        public CruizeCaribbean(int cabinNumber, string hint) : base(hint)
        {
            CabinNumber = cabinNumber;
        }
    }
    public class HorseRiding : Prize
    {
        public int Length { get; private set; }
        public HorseRiding(int length, string hint) : base(hint)
        {
            Length = length;
        }
    }
}
