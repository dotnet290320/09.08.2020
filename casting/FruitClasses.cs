﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _090820
{
    public abstract class Fruit
    {
        public string Name { get; set; }
        public abstract int Calories { get; }
        public abstract int GetCalories();
    }

    public class Banana : Fruit
    {
        private string _banana_katif_name;
        // my field (not backing field!)
        public string BananaKatifName
        {
            get
            {
                return _banana_katif_name;
            }
            set
            {
                if (_banana_katif_name != "")
                    _banana_katif_name = value;
            }
        }

        public bool IsStillYellow { get; set; }
        //private int _calories = 120;
        public override int Calories
        {
            get
            {
                // 1 value will not change -- return 120
                // 2 i want to allow only get access!!!!
                return 120;
            }
        }

        public Banana(bool isYellow)
        {
            Name = "banana";
            IsStillYellow = isYellow;
        }

        public override int GetCalories()
        {
            return 120;
        }
    }
    public class Orange : Fruit
    {
        public Orange()
        {
            Name = "Orange";
        }

        // there is no backing field
        // the get logic is implemented manually
        public override int Calories
        {
            get
            {
                return 80;
            }
        }
        public override int GetCalories()
        {
            return 80;
        }
    }

}
