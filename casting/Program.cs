﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _090820
{
    class Program
    {
        static void MakeSalad(Fruit f)
        {
            Console.WriteLine($"using {f.Name}");
            Console.WriteLine($"calories = {f.GetCalories()}");
            Console.WriteLine($"calories = {f.Calories}");

            // if the fruit is banana, then print if its yellow or not
            // is
            // as
            // casting
            // GetType
            if (f is Banana)
            {
                Console.WriteLine("I got the banana fruit into this function!");
                Banana banana2 = (Banana)f; // casting [safe]
                Console.WriteLine($"katif name = {banana2.BananaKatifName}");
                
            }
            else
            {
                Console.WriteLine("I did NOT got the banana fruit into this function!");
            }
            if (f.GetType().Name == "Banana")
            {
                Console.WriteLine("I got the banana fruit into this function!");
            }
            else
            {
                Console.WriteLine("I did NOT got the banana fruit into this function!");
            }

            Console.WriteLine("=====================================");
            // casting
            // safe-casting
            // check if possible and only then perform casting
            // unsafe-casting
            // perform casting immediately!
            // if you chose wrong the app will crash!!

            // bad: casting without checking!!!
            //Banana banana = (Banana)f; // casting [unsafe] -- could explode!

            // casting down casting is for using derived classes:
            //      methods, properties, fields ....

            // as
            Console.WriteLine("Look here!");
            Banana try_banana = f as Banana; // never explode!!
            // if f is not Banana --> try_banana == null
            if (try_banana == null)
            {
                Console.WriteLine("this is not a banana!");
            }
            else
            {
                Console.WriteLine($"katif name = {try_banana.BananaKatifName}");
            }

        }
        static void Main(string[] args)
        {
            Banana b = new Banana(true);
            Fruit banan_as_fruit = new Banana(true);
            //banan_as_fruit.Name
            MakeSalad(banan_as_fruit);

            //MakeSalad(new Banana(true));
           // MakeSalad(new Banana(false));
            //MakeSalad(new Banana(false) { KatifName = "negba" });

            // 1
            Orange o = new Orange();
            MakeSalad(o);
            // 2 -- same as 1 , just a shortcut
            //MakeSalad(new Orange());
        }
    }
}
